﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CALCULADORA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        string operacion;
        double signo, num1, num2, resultado;

        private void btnTres_Click(object sender, EventArgs e)  // Con los numeros uso el if para que cuando el texto sea igual a 0 me coloque el numero que le corresponde
                                                                // y asi mismo con el else me sirve para que cada vez que le de al mismo numero me añada otro del mismo. Asi con todos los numeros.
        {
            if (txtResultado.Text == "0")
            {
                txtResultado.Text = "3";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "3";
            }
        }

        private void btnCuatro_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text == "0")
            {
                txtResultado.Text = "4";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "4";
            }
        }

        private void btnCinco_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text == "0")
            {
                txtResultado.Text = "5";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "5";
            }
        }

        private void btnSeis_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text == "0")
            {
                txtResultado.Text = "6";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "6";
            }
        }

        private void btnSiete_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text == "0")
            {
                txtResultado.Text = "7";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "7";
            }
        }

        private void btnOcho_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text == "0")
            {
                txtResultado.Text = "8";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "8";
            }
        }

        private void btnNueve_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text == "0")
            {
                txtResultado.Text = "9";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "9";
            }
        }

        private void txtResultado_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void btnEliminar_Click(object sender, EventArgs e) // Para eliminar, le indico que cada vez al pulsar el botos le de un valor de 0 a todo y asi poder comenzar de 0 las opreciones.
        {
            txtResultado.Text = "0";
            num1 = 0;
            num2 = 0;
            resultado = 0;
        }
         
        private void btnDividir_Click(object sender, EventArgs e) // Aqui le estamos dando un valor de division al boton asi cuando este usando la calculadora y le de a divir me ponga un 0 para añadir el otro numero y hacer la operacion. Lo mismo con las demas operaciones.
        {
            num1 = Double.Parse(txtResultado.Text);
            txtResultado.Text = "0";
            operacion = "/";

        }

        private void btnMenos_Click(object sender, EventArgs e)
        {
            num1 = Double.Parse(txtResultado.Text);
            txtResultado.Text = "0";
            operacion = "-";
        }

        private void btnDos_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text == "0")
            {
                txtResultado.Text = "2";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "2";
            }
        }

        private void btnMultiplicar_Click(object sender, EventArgs e)
        {
            num1 = Double.Parse(txtResultado.Text);
            txtResultado.Text = "0";
            operacion = "*";
        }

        private void btnMas_Click(object sender, EventArgs e)
        {
            num1 = Double.Parse(txtResultado.Text);
            txtResultado.Text = "0";
            operacion = "+";
        }

        private void btnUno_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text == "0")
            {
                txtResultado.Text = "1";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "1";
            }
        }

        private void btnCero_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text == "0")
            {
                txtResultado.Text = "0";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "0";
            }
        }

        private void btnSigno_Click(object sender, EventArgs e)
        {
            signo = double.Parse(txtResultado.Text); // 15
            signo = signo - (signo * 2);             // signo = -15 - (15 * 2) -> -15 - (-30)
            txtResultado.Text = signo.ToString();    // signo = 15 --- Cada vez que le demos podremos cambiar de signo a nuestro numero.
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {

            num2 = double.Parse(txtResultado.Text);
            if (operacion == "+")
                txtResultado.Text = Convert.ToString(num1 + num2); // Cuando le demos al igual, nos empezata a hacer las opciones q nosotros le hayamos propuesto. En este caso seria una suma.
            else
            if (operacion == "-")
                txtResultado.Text = Convert.ToString(num1 - num2); // Cuando le demos al igual, nos empezata a hacer las opciones q nosotros le hayamos propuesto. En este caso seria una resta.
            else
            if (operacion == "*")
                txtResultado.Text = Convert.ToString(num1 * num2); // Cuando le demos al igual, nos empezata a hacer las opciones q nosotros le hayamos propuesto. En este caso seria una multiplicacion..
            if (operacion == "/")
            {
                txtResultado.Text = Convert.ToString(num1 / num2); // En el caso de la division, al calcular nuestros numeros, el segundo if se fijara q si pongo un 0 no comentara q la division no es posible de hacerse. Con los otros numero dividira correctamente.
                if (num2 == 0)
                    MessageBox.Show("No se realizo la operacion");
                
            }
        }
    }
} 
